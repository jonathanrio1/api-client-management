openapi: '3.0.1'
info:
  title: Client Management Services
  version: 1.0.0
  description: Client Management Services
  contact:
    name: Jonathan RIO
tags:
  - name: Client Management Services
servers:
  - url: http://localhost
    description: Local environnement

paths:
  /services/v1/createMessage:
    post:
      tags:
        - Client Management Services
      summary: Create a message
      operationId: createMessage
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MessageRequest'
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MessageResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /services/v1/getMessagesByAuthor/{author}:
    get:
      tags:
        - Client Management Services
      summary: Get messages by author
      operationId: getMessagesByAuthor
      parameters:
        - name: author
          in: path
          description: 'Author name'
          required: true
          schema:
             type: string
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MessagesAuthorResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /services/v1/createClientRecordFromMessage:
    post:
      tags:
        - Client Management Services
      summary: Create client record from a message
      operationId: createClientRecordFromMessage
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ClientRecordRequest'
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ClientRecordResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /services/v1/replyMessage:
    post:
      tags:
        - Client Management Services
      summary: reply to a message
      operationId: replyMessage
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ReplyMessageRequest'
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MessageResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /services/v1/updateClientRecord:
    post:
      tags:
        - Client Management Services
      summary: Update client record
      operationId: updateClientRecord
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ClientRecordUpdateRequest'
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ClientRecordUpdateResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /services/v1/getClientsRecords:
    get:
      tags:
        - Client Management Services
      summary: get clients records
      operationId: getClientsRecords
      responses:
        200:
          description: The response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ClientsRecordsResponse'
        401:
          description: Unauthorized.
        403:
          description: Forbidden.
        404:
          description: Resource does not exist.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  
components:
  schemas:
    ApiError:
      type: object
      properties:
        error:
          type: string
        internalErrorCode:
          type: string
        message:
          type: string
        path:
          type: string
        status:
          type: integer
          format: int32
        timestamp:
          type: string
          format: date-time
    
    MessageRequest:
      type: object
      required:
        - author
        - message
        - canal
      properties:
        author:
          type: string
          description: Author name
          example: John Doe
        message:
          type: string
          description: Message content
          example: Bonjour, j’ai un problème avec mon nouveau téléphone
        canal:
          type: string
          enum: [MAIL, SMS, FACEBOOK, TWITTER]
    
    MessageResponse:
      type: object
      properties:
        idMessage:
          type: integer
          format: int32
          description: message ID
        createdDate:
          type: string
          format: date-time
          description: Creation date for the message
          example: 2021-01-30T08:30:00Z
        message:
          $ref: '#/components/schemas/MessageRequest'

    MessagesAuthorResponse:
      description: List of messages for an author
      type: array
      items:
        $ref: "#/components/schemas/MessageResponse"

    ClientRecordRequest:
      type: object
      required:
        - reference
        - name
        - idMessage
      properties:
        reference:
          type: string
          description: Client reference
          example: KA-18B6
        name:
          type: string
          description: Client name
          example: John Doe
        idMessage:
          type: integer
          format: int32
          description: The message ID

    ClientRecordResponse:
      type: object
      properties:
        idClient:
          type: integer
          format: int32
          description: record ID
        reference:
          type: string
          description: Client reference
          example: KA-18B6
        name:
          type: string
          description: Client name
          example: John Doe
        createdDate:
          type: string
          format: date-time
          description: Created date for the client record
          example: 2021-01-30T08:30:00Z
        updatedDate:
          type: string
          format: date-time
          description: Updated date for the client record
          example: 2021-01-30T08:30:00Z
        messages:
          type: array
          items:
            $ref: '#/components/schemas/MessageResponse'

    ClientRecordUpdateRequest:
      type: object
      required:
        - reference
      properties:
        oldReference:
          type: string
          description: Actual client reference to change
          example: KA-18B6
        reference:
          type: string
          description: New client reference
          example: KA-18B7
        oldName:
          type: string
          description: Client name
          example: John Doe
        name:
          type: string
          description: Client name
          example: John Adam Doe

    ClientRecordUpdateResponse:
      type: object
      properties:
        updatedDate:
          type: string
          format: date-time
          description: Updated date for the client record
          example: 2021-01-30T08:30:00Z
        record:
          $ref: '#/components/schemas/ClientRecordUpdateRequest'

    ReplyMessageRequest:
      type: object
      required:
         - reference
         - message
      properties:
        reference:
          type: string
          description: Client reference
          example: KA-18B6
        message:
          $ref: '#/components/schemas/MessageRequest'

    ReplyMessageResponse:
      type: object
      properties:
        idMessage:
          type: integer
          format: int32
          description: message ID
        date:
          type: string
          format: date-time
          description: Creation date for the message
          example: 2021-01-30T08:30:00Z
        message:
          $ref: '#/components/schemas/ReplyMessageRequest'

    ClientsRecordsResponse:
          description: List of clients records
          type: array
          items:
            $ref: "#/components/schemas/ClientRecordResponse"
