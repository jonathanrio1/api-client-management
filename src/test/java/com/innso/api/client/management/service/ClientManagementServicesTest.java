package com.innso.api.client.management.service;

import static org.junit.Assert.assertEquals;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.innso.api.client.management.model.ClientRecordRequest;
import com.innso.api.client.management.model.ClientRecordResponse;
import com.innso.api.client.management.model.MessageRequest;
import com.innso.api.client.management.model.MessageRequest.CanalEnum;
import com.innso.api.client.management.model.MessageResponse;
import com.innso.api.client.management.repository.dao.ClientRecordDAO;
import com.innso.api.client.management.repository.dao.MessageDAO;
import com.innso.api.client.management.repository.entity.ClientRecordEntity;
import com.innso.api.client.management.repository.entity.MessageEntity;

@RunWith(SpringRunner.class)
@ActiveProfiles("junit")
public class ClientManagementServicesTest {

	@InjectMocks
	ClientManagementServices services;

	@Mock
	MessageDAO messageDAO;

	@Mock
	ClientRecordDAO clientRecordDAO;

	@Mock
	ModelMapper modelMapper;

	@Before
	public void init() {
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setIdMessage(1);
		messageEntity.setAuthor("John Doe");
		messageEntity.setCanal("MAIL");
		messageEntity.setDate(OffsetDateTime.now());
		messageEntity.setMessage("Message test");

		Mockito.when(messageDAO.findMessageById(1)).thenReturn(messageEntity);
		Mockito.when(messageDAO.save(Mockito.any(MessageEntity.class))).thenReturn(messageEntity);
		Mockito.when(modelMapper.map(messageEntity, MessageEntity.class)).thenReturn(new MessageEntity());

		ClientRecordEntity clientRecordEntity = new ClientRecordEntity();
		clientRecordEntity.setIdClient(1);
		clientRecordEntity.setName("John Doe");
		clientRecordEntity.setReference("JD-142");
		clientRecordEntity.setCreatedDate(OffsetDateTime.now());
		clientRecordEntity.setUpdatedDate(OffsetDateTime.now());

		List<MessageEntity> messages = new ArrayList<MessageEntity>();
		messages.add(messageEntity);

		clientRecordEntity.setMessages(messages);

		Mockito.when(clientRecordDAO.findClientRecordByRef("JD-142")).thenReturn(null);
		Mockito.when(clientRecordDAO.save(Mockito.any(ClientRecordEntity.class))).thenReturn(clientRecordEntity);
		Mockito.when(modelMapper.map(clientRecordEntity, ClientRecordEntity.class)).thenReturn(new ClientRecordEntity());
	}

	@Test
	public void testCreateMessage() {
		// Execute service.
		MessageRequest messageRequest = new MessageRequest();
		messageRequest.setAuthor("John Doe");
		messageRequest.setMessage("Message test");
		messageRequest.setCanal(CanalEnum.MAIL);
		MessageResponse messageResponse = services.createMessage(messageRequest);

		// Check the result.
		assertEquals(Integer.valueOf(1), messageResponse.getIdMessage());
	}

	@Test
	public void testCreateClientRecordFromMessage() {
		// Execute service.
		ClientRecordRequest clientRequest = new ClientRecordRequest();
		clientRequest.setIdMessage(1);
		clientRequest.setReference("JD-142");
		clientRequest.setName("John Doe");
		ClientRecordResponse clientResponse = services.createClientRecordFromMessage(clientRequest);

		// Check the result.
		assertEquals(Integer.valueOf(1), clientResponse.getIdClient());
	}
}
