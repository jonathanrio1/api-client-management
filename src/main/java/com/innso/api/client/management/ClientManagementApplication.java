package com.innso.api.client.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "com.innso.api.client.management" })
public class ClientManagementApplication {

	public static void main(String[] args) {
		new SpringApplication(ClientManagementApplication.class).run(args);
	}

}
