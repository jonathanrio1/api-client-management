package com.innso.api.client.management.exception;

public class NoDataFoundException extends RuntimeException {

	/** Serial UI. */
	private static final long serialVersionUID = 1L;

	public NoDataFoundException(final String message) {
		super(message);
	}

}
