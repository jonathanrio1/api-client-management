package com.innso.api.client.management.exception;

public class PersistenceException extends RuntimeException {

	/** Serial UI. */
	private static final long serialVersionUID = 1L;

	public PersistenceException(final String message) {
		super(message);
	}

}
