package com.innso.api.client.management.web;

import java.time.OffsetDateTime;
import java.util.concurrent.ExecutionException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.innso.api.client.management.exception.InputParameterException;
import com.innso.api.client.management.exception.NoDataFoundException;
import com.innso.api.client.management.exception.PersistenceException;
import com.innso.api.client.management.model.ApiError;

import lombok.extern.slf4j.Slf4j;

/**
 * Centralization of exception handling.
 */
@RestControllerAdvice
@CrossOrigin("*")
@Slf4j
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * This root cause is throwed if bad request
	 *
	 * @param exception : The exception
	 * @param request   : the Web request.
	 * @return a {@link ResponseEntity} object.
	 */
	@ExceptionHandler({ InputParameterException.class })
	public ResponseEntity<Object> handleInputParameterException(InputParameterException exception, WebRequest request) {
		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		String message = exception.getMessage();
		log.error(message);
		ApiError apiError = createApiError(httpStatus, message);
		return new ResponseEntity<>(apiError, new HttpHeaders(), httpStatus);
	}

	/**
	 * This root cause is throwed if bad request
	 *
	 * @param exception : The exception
	 * @param request   : the Web request.
	 * @return a {@link ResponseEntity} object.
	 */
	@ExceptionHandler({ NoDataFoundException.class })
	public ResponseEntity<Object> handleNoDataFoundException(NoDataFoundException exception, WebRequest request) {
		HttpStatus httpStatus = HttpStatus.NOT_FOUND;
		String message = exception.getMessage();
		log.error(message);
		ApiError apiError = createApiError(httpStatus, message);
		return new ResponseEntity<>(apiError, new HttpHeaders(), httpStatus);
	}

	/**
	 * This root cause is throwed if bad request
	 *
	 * @param exception : The exception
	 * @param request   : the Web request.
	 * @return a {@link ResponseEntity} object.
	 */
	@ExceptionHandler({ PersistenceException.class })
	public ResponseEntity<Object> handlePersistenceException(PersistenceException exception, WebRequest request) {
		HttpStatus httpStatus = HttpStatus.NOT_FOUND;
		String message = exception.getMessage();
		log.error(message);
		ApiError apiError = createApiError(httpStatus, message);
		return new ResponseEntity<>(apiError, new HttpHeaders(), httpStatus);
	}

	/**
	 * This root cause is linked with the multi-threading execution : 500 INTERNAL_SERVER_ERROR
	 *
	 * @param exception : The exception
	 * @param request   : the Web request.
	 * @return a {@link ResponseEntity} object.
	 */
	@ExceptionHandler({ ExecutionException.class })
	public ResponseEntity<Object> handleExecutionException(ExecutionException exception, WebRequest request) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		String message = exception.getMessage();
		log.error(message);
		ApiError apiError = createApiError(httpStatus, message);
		return new ResponseEntity<>(apiError, new HttpHeaders(), httpStatus);
	}

	/**
	 * This root cause is linked with the multi-threading execution : 500 INTERNAL_SERVER_ERROR
	 *
	 * @param exception : The exception
	 * @param request   : the Web request.
	 * @return a {@link ResponseEntity} object.
	 */
	@ExceptionHandler({ InterruptedException.class })
	public ResponseEntity<Object> handleInterruptedException(InterruptedException exception, WebRequest request) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		String message = exception.getMessage();
		log.error(message);
		ApiError apiError = createApiError(httpStatus, message);
		return new ResponseEntity<>(apiError, new HttpHeaders(), httpStatus);
	}

	/**
	 * This method create a generic structure error.
	 * 
	 * @param status            : The status code of the request.
	 * @param path              : The path error.
	 * @param timestamp         : A timestamp.
	 * @param internalErrorCode : The internal code error.
	 * @param message           : The error message.
	 * @return a {ApiError} message.
	 */
	private ApiError createApiError(HttpStatus status, OffsetDateTime timestamp, String message) {
		ApiError apiError = new ApiError();
		apiError.setStatus(status.value());
		apiError.setError(status.name());
		apiError.setTimestamp(timestamp);
		apiError.setMessage(message);
		return apiError;
	}

	/**
	 * This method create a generic structure error.
	 * 
	 * @param status            : The status code of the request.
	 * @param internalErrorCode : The internal code error.
	 * @param message           : The error message.
	 * @return a {ApiError} message.
	 */
	public ApiError createApiError(HttpStatus status, String message) {
		return createApiError(status, OffsetDateTime.now(), message);
	}

}
