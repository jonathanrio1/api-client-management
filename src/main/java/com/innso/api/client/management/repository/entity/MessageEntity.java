package com.innso.api.client.management.repository.entity;

import java.time.OffsetDateTime;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "MESSAGE")
@Embeddable
public class MessageEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idMessage;

	@NotNull
	private String author;

	@NotNull
	private String message;

	@NotNull
	private String canal;

	@NotNull
	private OffsetDateTime createdDate;

	private int idClient;

	public MessageEntity() {
		// Default constructor
	}

	public MessageEntity(String author, String message, String canal, OffsetDateTime createdDate) {
		this.author = author;
		this.message = message;
		this.canal = canal;
		this.createdDate = createdDate;
	}

	public MessageEntity(String author, String message, String canal, OffsetDateTime createdDate, int idClient) {
		this.author = author;
		this.message = message;
		this.canal = canal;
		this.createdDate = createdDate;
		this.idClient = idClient;
	}

	public int getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(int idMessage) {
		this.idMessage = idMessage;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public OffsetDateTime getCreatedDate() {
		return createdDate;
	}

	public void setDate(OffsetDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

}
