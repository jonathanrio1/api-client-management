package com.innso.api.client.management.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.innso.api.client.management.repository.entity.MessageEntity;

@Repository
public interface MessageDAO extends CrudRepository<MessageEntity, Integer> {

	@Query("select m from MessageEntity m where m.idMessage = :idMessage")
	public MessageEntity findMessageById(int idMessage);

	@Query("select m from MessageEntity m where m.idClient = :idClient")
	public List<MessageEntity> findMessagesByIdClient(int idClient);

	@Query("select m from MessageEntity m where lower(m.author) = :author")
	public List<MessageEntity> findMessagesByAuthor(String author);

	@Modifying
	@Query("update MessageEntity set idClient = :idClient where idMessage = :idMessage")
	void updateMessageClientRecordById(@Param("idClient") int idClient, @Param("idMessage") int idMessage);

}
