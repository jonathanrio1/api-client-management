package com.innso.api.client.management.service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.innso.api.client.management.exception.InputParameterException;
import com.innso.api.client.management.exception.NoDataFoundException;
import com.innso.api.client.management.model.ClientRecordRequest;
import com.innso.api.client.management.model.ClientRecordResponse;
import com.innso.api.client.management.model.ClientRecordUpdateRequest;
import com.innso.api.client.management.model.ClientRecordUpdateResponse;
import com.innso.api.client.management.model.MessageRequest;
import com.innso.api.client.management.model.MessageRequest.CanalEnum;
import com.innso.api.client.management.model.MessageResponse;
import com.innso.api.client.management.model.ReplyMessageRequest;
import com.innso.api.client.management.repository.dao.ClientRecordDAO;
import com.innso.api.client.management.repository.dao.MessageDAO;
import com.innso.api.client.management.repository.entity.ClientRecordEntity;
import com.innso.api.client.management.repository.entity.MessageEntity;

@Service
@Transactional
public class ClientManagementServices {

	private Logger log = LoggerFactory.getLogger(ClientManagementServices.class);

	@Autowired
	private MessageDAO messageDAO;

	@Autowired
	private ClientRecordDAO clientRecordDAO;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Création d'un message client
	 * 
	 * @param MessageRequest
	 * @return MessageResponse
	 */
	public MessageResponse createMessage(MessageRequest messageRequest) {

		if (StringUtils.isBlank(messageRequest.getAuthor()) && StringUtils.isBlank(messageRequest.getMessage())
				&& StringUtils.isBlank(messageRequest.getCanal().getValue())) {
			log.error("One of these fields is null or empty : author / message / canal");
			throw new InputParameterException("One of these fields is null or empty : author / message / canal");
		}

		log.info(String.format("Creating the message : %s", messageRequest.toString()));

		OffsetDateTime dateTime = OffsetDateTime.now();

		// Enregistrement du message
		MessageEntity messageSaved = messageDAO.save(
				new MessageEntity(messageRequest.getAuthor(), messageRequest.getMessage(), messageRequest.getCanal().getValue(), dateTime));

		if (messageSaved == null) {
			log.error("Error saving the message datas into the database");
			throw new InputParameterException("Error saving the message datas into the database");
		}

		MessageResponse response = new MessageResponse();
		response.setIdMessage(messageSaved.getIdMessage());
		response.setCreatedDate(dateTime);
		response.setMessage(messageRequest);

		log.info(String.format("The message has been saved successfully : %s", messageSaved.getIdMessage()));

		return response;

	}

	/**
	 * Récupération des messages par auteur
	 * 
	 * @param author
	 * @return List<MessageResponse>
	 */
	public List<MessageResponse> getMessagesByAuthor(String author) {

		if (StringUtils.isBlank(author)) {
			log.error("The field 'author' is null or empty");
			throw new InputParameterException("The field 'author' is null or empty");
		}

		log.info(String.format("Getting the messages for the author : %s", author));

		// Récupération des messages associés à un auteur
		List<MessageEntity> messagesEntity = messageDAO.findMessagesByAuthor(author.toLowerCase());

		if (messagesEntity.isEmpty()) {
			log.info(String.format("No message found into the database for the author %s", author));
			throw new NoDataFoundException(String.format("No message found into the database for the author %s", author));
		}

		List<MessageResponse> messagesResponse = messagesEntity.stream()
				.map(messageEntity -> modelMapper.map(messageEntity, MessageResponse.class)).collect(Collectors.toList());

		log.info(String.format("%s messages retrieved for the author %s", messagesResponse.size(), author));

		return messagesResponse;
	}

	/**
	 * Création d'un dossier client à partir d'un message
	 * 
	 * @param ClientRecordRequest
	 * @return ClientRecordResponse
	 */
	public ClientRecordResponse createClientRecordFromMessage(ClientRecordRequest clientRecordsRequest) {

		if (StringUtils.isBlank(clientRecordsRequest.getReference()) && StringUtils.isBlank(clientRecordsRequest.getName())
				&& clientRecordsRequest.getIdMessage() == 0) {
			log.error("One of these fields is null or empty : reference / name / idMessage");
			throw new InputParameterException("One of these fields is null or empty : reference / name / idMessage");
		}

		// On vérifie si le dossier client n'existe pas déjà
		ClientRecordEntity recordSearch = clientRecordDAO.findClientRecordByRef(clientRecordsRequest.getReference());

		if (recordSearch != null) {
			log.error(String.format("The client record already exists : %s", clientRecordsRequest.getReference()));
			throw new InputParameterException(String.format("The client record already exists : %s", clientRecordsRequest.getReference()));
		}

		// Recherche du message
		MessageEntity messageSearchEntity = messageDAO.findMessageById(clientRecordsRequest.getIdMessage());

		if (messageSearchEntity == null) {
			log.info(String.format("No message found for the id : %s", clientRecordsRequest.getIdMessage()));
			throw new NoDataFoundException(String.format("No message found for the id : %s", clientRecordsRequest.getIdMessage()));
		}

		log.info(String.format("Creating the client record with message : %s", clientRecordsRequest.toString()));
		OffsetDateTime dateTime = OffsetDateTime.now();

		// Si le dossier client n'existe pas déjà, on créé le nouveau dossier
		ClientRecordEntity recordSaved = clientRecordDAO
				.save(new ClientRecordEntity(clientRecordsRequest.getReference(), clientRecordsRequest.getName(), dateTime, dateTime));

		if (recordSaved == null) {
			log.error("Error saving the client record into the database");
			throw new InputParameterException("Error saving the client record into the database");
		}

		// Mise à jour du message avec l'ID client
		messageDAO.updateMessageClientRecordById(recordSaved.getIdClient(), clientRecordsRequest.getIdMessage());

		ClientRecordResponse response = new ClientRecordResponse();
		response.setIdClient(recordSaved.getIdClient());
		response.setReference(clientRecordsRequest.getReference());
		response.setName(clientRecordsRequest.getName());
		response.setCreatedDate(dateTime);
		response.setUpdatedDate(dateTime);

		MessageRequest messageRequest = new MessageRequest();
		messageRequest.setAuthor(messageSearchEntity.getAuthor());
		messageRequest.setMessage(messageSearchEntity.getMessage());
		messageRequest.setCanal(CanalEnum.valueOf(messageSearchEntity.getCanal()));

		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setIdMessage(clientRecordsRequest.getIdMessage());
		messageResponse.setCreatedDate(messageSearchEntity.getCreatedDate());
		messageResponse.setMessage(messageRequest);

		List<MessageResponse> messages = new ArrayList<MessageResponse>();
		messages.add(messageResponse);

		response.setMessages(messages);

		log.info(String.format("The client record has been saved successfully : %s", recordSaved.getIdClient()));

		return response;
	}

	/**
	 * Réponse à un client
	 * 
	 * @param ReplyMessageRequest
	 * @return MessageResponse
	 */
	public MessageResponse replyMessage(ReplyMessageRequest replyMessageRequest) {

		if (StringUtils.isBlank(replyMessageRequest.getReference()) && replyMessageRequest.getMessage() == null) {
			log.error("One of these fields is null or empty : reference / message");
			throw new InputParameterException("One of these fields is null or empty : reference / message");
		}

		log.info(String.format("Replying to the client : %s", replyMessageRequest.toString()));

		// Recherche du dossier client depuis la reference
		ClientRecordEntity recordSearchEntity = clientRecordDAO.findClientRecordByRef(replyMessageRequest.getReference());

		if (recordSearchEntity == null) {
			log.info(String.format("No client record found for the reference : %s", replyMessageRequest.getReference()));
			throw new NoDataFoundException(
					String.format("No client record found for the reference : %s", replyMessageRequest.getReference()));
		}

		OffsetDateTime dateTime = OffsetDateTime.now();

		// Enregistrement de la réponse avec l'ID du client
		MessageEntity messageSaved = messageDAO
				.save(new MessageEntity(replyMessageRequest.getMessage().getAuthor(), replyMessageRequest.getMessage().getMessage(),
						replyMessageRequest.getMessage().getCanal().getValue(), dateTime, recordSearchEntity.getIdClient()));

		if (messageSaved == null) {
			log.info("Error saving the reply into the database");
			throw new NoDataFoundException("Error saving the reply into the database");
		}

		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setIdMessage(messageSaved.getIdMessage());
		messageResponse.setCreatedDate(dateTime);
		messageResponse.setMessage(replyMessageRequest.getMessage());

		log.info(String.format("The reply has been updated successfully : %s", messageSaved.getIdMessage()));

		return messageResponse;
	}

	/**
	 * Mise à jour d'un dossier client
	 * 
	 * @param ClientRecordRequest
	 * @return ClientRecordUpdateResponse
	 */
	public ClientRecordUpdateResponse updateClientRecord(ClientRecordUpdateRequest clientRecordRequest) {

		if ((StringUtils.isBlank(clientRecordRequest.getOldReference()) && StringUtils.isBlank(clientRecordRequest.getReference()))
				|| (StringUtils.isBlank(clientRecordRequest.getOldName()) && StringUtils.isBlank(clientRecordRequest.getName()))) {
			log.error("One of these pairs fields is null or empty : oldReference - reference / oldName - name");
			throw new InputParameterException("One of these pairs fields is null or empty : oldReference - reference / oldName - name");
		}

		log.info(String.format("Updating the client record : %s", clientRecordRequest.toString()));

		boolean updateReference = false;
		boolean updateName = false;

		if (StringUtils.isNotBlank(clientRecordRequest.getOldReference()) && StringUtils.isNotBlank(clientRecordRequest.getReference())) {
			updateReference = true;
		}
		if (StringUtils.isNotBlank(clientRecordRequest.getOldName()) && StringUtils.isNotBlank(clientRecordRequest.getName())) {
			updateName = true;
		}

		// Recherche du dossier client
		ClientRecordEntity recordSearchEntity = clientRecordDAO.findClientRecordByRef(clientRecordRequest.getOldReference());

		if (recordSearchEntity == null) {
			log.info(String.format("No client record found for the reference : %s", clientRecordRequest.getOldReference()));
			throw new NoDataFoundException(
					String.format("No client record found for the reference : %s", clientRecordRequest.getOldReference()));
		}

		OffsetDateTime dateTime = OffsetDateTime.now();

		// Si le dossier client a été trouvé, on met à jour les données
		if (updateReference && updateName) {
			clientRecordDAO.updateReferenceAndNameClientRecordByRef(clientRecordRequest.getReference(), clientRecordRequest.getName(),
					dateTime, clientRecordRequest.getOldReference());
		} else if (updateReference && !updateName) {
			clientRecordDAO.updateReferenceClientRecordByRef(clientRecordRequest.getReference(), dateTime,
					clientRecordRequest.getOldReference());
		} else {
			log.info(String.format("The conditions are not met to update the client record : %s", clientRecordRequest.getOldReference()));
			throw new InputParameterException(
					String.format("The conditions are not met to update the client record : %s", clientRecordRequest.getOldReference()));
		}

		ClientRecordUpdateResponse updateResponse = new ClientRecordUpdateResponse();
		updateResponse.setUpdatedDate(dateTime);
		updateResponse.setRecord(clientRecordRequest);

		log.info(String.format("The client record has been updated successfully : %s", clientRecordRequest.getReference()));

		return updateResponse;
	}

	/**
	 * Récupération des dossiers clients
	 * 
	 * @return List<ClientRecordsResponse>
	 */
	public List<ClientRecordResponse> getClientsRecords() {

		log.info("Getting the clients records");

		// Récupération de tous les dossiers clients
		List<ClientRecordEntity> clientsRecordsEntity = clientRecordDAO.findAll();

		if (clientsRecordsEntity.isEmpty()) {
			log.info("No client record found into the database");
			throw new NoDataFoundException("No client record found into the database");
		}

		for (ClientRecordEntity client : clientsRecordsEntity) {
			List<MessageEntity> messages = messageDAO.findMessagesByIdClient(client.getIdClient());
			client.setMessages(messages);
		}

		List<ClientRecordResponse> clientsRecordsResponse = clientsRecordsEntity.stream()
				.map(clientEntity -> modelMapper.map(clientEntity, ClientRecordResponse.class)).collect(Collectors.toList());

		log.info(String.format("%s clients records retrieved", clientsRecordsResponse.size()));

		return clientsRecordsResponse;
	}

}
