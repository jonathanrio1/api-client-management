package com.innso.api.client.management.web;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;

import com.innso.api.client.management.handler.ServicesApi;
import com.innso.api.client.management.model.ClientRecordRequest;
import com.innso.api.client.management.model.ClientRecordResponse;
import com.innso.api.client.management.model.ClientRecordUpdateRequest;
import com.innso.api.client.management.model.ClientRecordUpdateResponse;
import com.innso.api.client.management.model.MessageRequest;
import com.innso.api.client.management.model.MessageResponse;
import com.innso.api.client.management.model.ReplyMessageRequest;
import com.innso.api.client.management.service.ClientManagementServices;

@Controller
public class ServicesController implements ServicesApi {

	ClientManagementServices clientManagementServices;

	public ServicesController(ClientManagementServices clientManagementServices) {
		this.clientManagementServices = clientManagementServices;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return ServicesApi.super.getRequest();
	}

	/**
	 * Création d'un message client
	 * 
	 * @param MessageRequest
	 * @return ResponseEntity<MessageResponse>
	 */
	@Override
	public ResponseEntity<MessageResponse> createMessage(MessageRequest messageRequest) {
		MessageResponse messageResponse = clientManagementServices.createMessage(messageRequest);
		return new ResponseEntity<>(messageResponse, HttpStatus.OK);
	}

	/**
	 * Récupération des messages par auteur
	 * 
	 * @param MessageAuthorRequest
	 * @return ResponseEntity<List<MessageResponse>>
	 */
	@Override
	public ResponseEntity<List<MessageResponse>> getMessagesByAuthor(String author) {
		List<MessageResponse> messagesResponse = clientManagementServices.getMessagesByAuthor(author);
		return new ResponseEntity<>(messagesResponse, HttpStatus.OK);
	}

	/**
	 * Création d'un dossier client à partir d'un message
	 * 
	 * @param ClientRecordRequest
	 * @return ResponseEntity<ClientRecordAddResponse>
	 */
	@Override
	public ResponseEntity<ClientRecordResponse> createClientRecordFromMessage(ClientRecordRequest clientRecordRequest) {
		ClientRecordResponse clientRecordResponse = clientManagementServices.createClientRecordFromMessage(clientRecordRequest);
		return new ResponseEntity<>(clientRecordResponse, HttpStatus.OK);
	}

	/**
	 * réponse à un message
	 * 
	 * @param ClientRecordAddMessageRequest
	 * @return ResponseEntity<ClientRecordAddMessageResponse>
	 */
	@Override
	public ResponseEntity<MessageResponse> replyMessage(ReplyMessageRequest replyMessageRequest) {
		MessageResponse messageResponse = clientManagementServices.replyMessage(replyMessageRequest);
		return new ResponseEntity<>(messageResponse, HttpStatus.OK);
	}

	/**
	 * Mise à jour d'un dossier client
	 * 
	 * @param ClientRecordRequest
	 * @return ResponseEntity<ClientRecordUpdateResponse>
	 */
	@Override
	public ResponseEntity<ClientRecordUpdateResponse> updateClientRecord(ClientRecordUpdateRequest clientRecordRequest) {
		ClientRecordUpdateResponse clientRecordResponse = clientManagementServices.updateClientRecord(clientRecordRequest);
		return new ResponseEntity<>(clientRecordResponse, HttpStatus.OK);
	}

	/**
	 * Récupération des dossiers clients
	 * 
	 * @return ResponseEntity<List<ClientRecordResponse>>
	 */
	@Override
	public ResponseEntity<List<ClientRecordResponse>> getClientsRecords() {
		List<ClientRecordResponse> clientsRecordsResponse = clientManagementServices.getClientsRecords();
		return new ResponseEntity<>(clientsRecordsResponse, HttpStatus.OK);
	}

}
