package com.innso.api.client.management.repository.dao;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.innso.api.client.management.repository.entity.ClientRecordEntity;

@Repository
public interface ClientRecordDAO extends CrudRepository<ClientRecordEntity, Integer> {

	@Query("select cr from ClientRecordEntity cr where cr.reference = :reference")
	public ClientRecordEntity findClientRecordByRef(String reference);

	@Modifying
	@Query("update ClientRecordEntity set reference = :reference, name = :name, updatedDate = :updatedDate where reference = :oldReference")
	void updateReferenceAndNameClientRecordByRef(@Param("reference") String reference, @Param("name") String name,
			@Param("updatedDate") OffsetDateTime updatedDate, @Param("oldReference") String oldReference);

	@Modifying
	@Query("update ClientRecordEntity set reference = :reference, updatedDate = :updatedDate where reference = :oldReference")
	void updateReferenceClientRecordByRef(@Param("reference") String reference, @Param("updatedDate") OffsetDateTime updatedDate,
			@Param("oldReference") String oldReference);

	public List<ClientRecordEntity> findAll();

}
