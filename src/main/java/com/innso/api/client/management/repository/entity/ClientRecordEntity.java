package com.innso.api.client.management.repository.entity;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CLIENT_RECORD")
public class ClientRecordEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idClient;

	@NotNull
	private String reference;

	@NotNull
	private String name;

	@NotNull
	private OffsetDateTime createdDate;

	@NotNull
	private OffsetDateTime updatedDate;

	@OneToMany
	List<MessageEntity> messages = new ArrayList<MessageEntity>();

	public ClientRecordEntity() {
		// Default constructor
	}

	public ClientRecordEntity(String reference, String name, OffsetDateTime createdDate, OffsetDateTime updatedDate) {
		this.reference = reference;
		this.name = name;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public ClientRecordEntity(String reference, String name, OffsetDateTime createdDate, OffsetDateTime updatedDate,
			List<MessageEntity> messages) {
		this.reference = reference;
		this.name = name;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.messages = messages;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OffsetDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(OffsetDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public OffsetDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(OffsetDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<MessageEntity> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageEntity> messages) {
		this.messages = messages;
	}
}
