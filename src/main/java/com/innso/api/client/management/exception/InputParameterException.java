package com.innso.api.client.management.exception;

public class InputParameterException extends RuntimeException {

	/** Serial UI. */
	private static final long serialVersionUID = 1L;

	public InputParameterException(final String message) {
		super(message);
	}

}
