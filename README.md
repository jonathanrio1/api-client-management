# API Client Management

## <a name="présentation">Présentation</a>
L'API Client Management expose les services REST permettant de créer et consulter des messages et dossiers clients.

## Table des matières
* [Configuration](#configurations)
* [Modèle base de données](#database-model)
* [Diagramme séquence](#diagramme-sequence)
* [Services](#services)

## <a name="configurations">Configurations</a>
* URL Swagger UI : http://localhost:8080/swagger-ui.html#!/
* Contrat de service OpenAPI 3.0.1 : https://gitlab.com/jonathanrio1/api-client-management/-/blob/master/doc/services-client-management.openapi.yaml

## <a name="database-model">Modèle base de données</a>
![database_model](doc/database_model.png)
## <a name="diagramme-sequence">Diagramme séquence</a>
![diagramme_sequence](doc/diagramme_sequence.png)
## <a name="services">Services</a>
#### *POST services/v1/createMessage*
##### Description
Ce service permet de créer un message avec son auteur et le canal de diffusion.

##### IN
````
{
  "author": "John Doe",
  "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
  "canal": "MAIL"
}
````

##### OUT
````
{
  "idMessage": 0,
  "createdDate": {},
  "message": {
    "author": "John Doe",
    "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
    "canal": "MAIL"
  }
}
````

#### *GET services/v1/getMessagesByAuthor/{author}*
##### Description
Ce service permet de retourner les messages associés à un auteur.

##### OUT
````
[
  {
    "idMessage": 0,
    "createdDate": {},
    "message": {
      "author": "John Doe",
      "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
      "canal": "MAIL"
    }
  }
]
````

#### *POST services/v1/createClientRecordFromMessage*
##### Description
Ce service permet de créer un dossier client et de rattacher un message.

##### IN
````
{
  "reference": "KA-18B6",
  "name": "John Doe",
  "idMessage": 0
}
````

##### OUT
````
{
  "idClient": 0,
  "reference": "KA-18B6",
  "name": "John Doe",
  "createdDate": {},
  "updatedDate": {},
  "messages": [
    {
      "idMessage": 0,
      "createdDate": {},
      "message": {
        "author": "John Doe",
        "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
        "canal": "MAIL"
      }
    }
  ]
}
````

#### *POST services/v1/replyMessage*
##### Description
Ce service permet de répondre au client en utilisant la référence du client.

##### IN
````
{
  "reference": "KA-18B6",
  "message": {
    "author": "John Doe",
    "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
    "canal": "MAIL"
  }
}
````

##### OUT
````
{
  "idMessage": 0,
  "createdDate": {},
  "message": {
    "author": "John Doe",
    "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
    "canal": "MAIL"
  }
}
````

#### *POST services/v1/updateClientRecord*
##### Description
Ce service permet de mettre à jour les données du client.

##### IN
````
{
  "oldReference": "KA-18B6",
  "reference": "KA-18B7",
  "oldName": "John Doe",
  "name": "John Adam Doe"
}
````

##### OUT
````
{
  "updatedDate": {},
  "record": {
    "oldReference": "KA-18B6",
    "reference": "KA-18B7",
    "oldName": "John Doe",
    "name": "John Adam Doe"
  }
}
````

#### *GET services/v1/getClientsRecords*
##### Description
Ce service permet de récupérer les dossiers clients et les messages associés.

##### OUT
````
[
  {
    "idClient": 0,
    "reference": "KA-18B6",
    "name": "John Doe",
    "createdDate": {},
    "updatedDate": {},
    "messages": [
      {
        "idMessage": 0,
        "createdDate": {},
        "message": {
          "author": "John Doe",
          "message": "Bonjour, j’ai un problème avec mon nouveau téléphone",
          "canal": "MAIL"
        }
      }
    ]
  }
]
````
